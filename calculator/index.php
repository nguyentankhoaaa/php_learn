<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
        <input type="number" name="num1" placeholder="Number One..." style="display: block;">
        <select name="operator" style="display: block; margin: 15px 0;">
            <option value="add">+</option>
            <option value="substract">-</option>
            <option value="multiply">*</option>
            <option value="devide">/</option>
        </select>
        <input style="display: block;margin: 15px 0;" type="number" name="num2" placeholder="Number Two...">
        <button type="submit">Calculate</button>
    </form>
</body>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $num1 = filter_input(INPUT_POST, "num1", FILTER_SANITIZE_NUMBER_FLOAT);
    $num2 = filter_input(INPUT_POST, "num2", FILTER_SANITIZE_NUMBER_FLOAT);
    $operator = htmlspecialchars($_POST["operator"]);
    $errors = false;
    if (empty($num1) || empty($num2) || empty($operator)) {
        echo "<p>Field in the fills</p>";
        $errors = true;
    };
    if (!is_numeric($num1) || !is_numeric($num2)) {
        echo "Please write number !";
    };
    if (!$errors) {
        $value = 0;
        switch ($operator) {
            case "add":
                $value =  $num1 + $num2;
                break;
            case "subtract":
                $value = $num1 - $num2;
                break;
            case "multiply":
                $value =  $num1 * $num2;
                break;
            case "devide":
                $value =  $num1 / $num2;
                break;
            default:
                echo "Somrthing went wrong!";
        }
        echo "$value";
    }
}
?>

</html>