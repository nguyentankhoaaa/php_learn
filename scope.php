<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $test = "Daniel";
    function myFunction()
    {
        static $staticVar = 0;
        $staticVar++;
        global $test;
        $localVar = "Hello World";
        return $staticVar;
    }
    echo myFunction();
    echo myFunction();


    class MyClass
    {
        public $classVar = "Hello World";
        public function myMethod()
        {
            echo $this->classVar;
        }
    }


    ?>
</body>

</html>