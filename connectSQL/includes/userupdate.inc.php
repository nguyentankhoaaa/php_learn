<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $email = $_POST["email"];
    try {
        require_once "dbh.inc.php";
        $query = "UPDATE users SET username=?,passwords=?,email=? WHERE id=1;";
        $stmt = $pdo->prepare($query);
        $stmt->execute([$username, $password, $email]);
        $stmt = null;
        $pdo = null;
        header("Location:../index.php");
        die();
    } catch (PDOException $e) {
        die("Required Failed:" . $e->getMessage());
    }
} else {
    header("Location:../index.php");
}
