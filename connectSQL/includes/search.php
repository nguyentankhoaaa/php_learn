<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $usersearch = $_POST["usersearch"];

    try {
        require_once "dbh.inc.php";
        $query = "SELECT * FROM comments where username=? ";
        $stmt = $pdo->prepare($query);
        $stmt->execute([$usersearch]);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $pdo = null;
        $stmt = null;
    } catch (PDOException $e) {
        die("Query failed:" . $e->getMessage());
    }
} else {
    header("Location:../index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h3>Search Result:</h3>
    <?php
    if (empty($results)) {
        echo "
        <p>
        There were no results!
        </p>
        ";
    } else {
        foreach ($results as $row) {
            echo  htmlspecialchars($row["username"]) . "<br>";
            echo htmlspecialchars($row["comments"]) . "<br>";
            echo htmlspecialchars($row["created_at"]);
        }
    }
    ?>
</body>

</html>