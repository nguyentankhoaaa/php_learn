<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST["email"];
    $password = $_POST["password"];
    try {
        require_once "dbh.inc.php";
        $query = 'DELETE FROM users where email=? AND passwords=?;';
        $stmt = $pdo->prepare($query);
        $stmt->execute([$email, $password]);
        $pdo = null;
        $stmt = null;
        header("Location:../index.php");
        die();
    } catch (PDOException $e) {
        die("Query Failed:" . $e->getMessage());
    }
} else {
    header("Location:../index.php");
}
