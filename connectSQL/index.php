<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div style="padding:0 40% ;">
        <form action="includes/formhandler.inc.php" method="post">
            <h3>Sign Up</h3>
            <input type="text" name="username" style="margin-bottom: 10px;" placeholder="Username...">
            <br>
            <input type="password" style="margin-bottom: 10px;" name="password" placeholder="Password...">
            <br>
            <input type="text" style="margin-bottom: 10px;" name="email" placeholder="E-mail">
            <br>
            <button type="submit">Signup</button>
        </form>
        <form style="margin-top: 40px;" action="includes/userupdate.inc.php" method="post">
            <h3>Change Account</h3>
            <input type="text" name="username" style="margin-bottom: 10px;" placeholder="Username...">
            <br>
            <input type="password" style="margin-bottom: 10px;" name="password" placeholder="Password...">
            <br>
            <input type="text" style="margin-bottom: 10px;" name="email" placeholder="E-mail...">
            <br>
            <button type="submit">Change Account</button>
        </form>
        <form style="margin-top: 40px;" action="includes/userdelete.inc.php" method="post">
            <h3>Delete Account</h3>
            <input type="text" style="margin-bottom: 10px;" name="email" placeholder="E-mail...">
            <br>
            <input type="password" style="margin-bottom: 10px;" name="password" placeholder="Password...">
            <br>
            <button type="submit">Delete</button>
        </form>
        <form style="margin-top: 40px;" action="includes/search.php" class="" method="post">
            <label for="search">Search for users:</label>
            <input type="text" id="search" name="usersearch" placeholder="Search...">
            <button type="submit">Search</button>
        </form>
    </div>
</body>

</html>